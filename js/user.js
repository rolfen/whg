
// contains all slideshows on page as slideshow objects
var slideshows = {};

// funcs 

var selectNews = function(filter) {
	// filter is a string
	// if filter is false (and non-string), then show all items (deselect any filter)
	// "select" or "show" means setting the class to "selected" on .news-items .item[tags] 
	// (note that tag attribute must be present for this function to do anything at all on the element)
	var selectedCount = 0
	var selectItem = function(elem) {
		addClass(elem,"selected")
		selectedCount ++
	}
	var deselectItem = function(elem) {
		removeClass(elem,"selected")
		// selectedCount --
	}
	var itemSelected = function(item) {
		return(hasClass(item,"selected"))
	}
	var showDefaultMessage = function(rootElem) {
		NW.Dom.select(".default-message",rootElem,function(elem){
			addClass(elem,"selected")
		})
	}
	var hideDefaultMessage = function(rootElem) {
		NW.Dom.select(".default-message",rootElem,function(elem){
			removeClass(elem,"selected")
		})
	}
	NW.Dom.select(".news-items .item[tags]",document,function(item){
		if(filter !== false) {
			var itemTags = item.getAttribute("tags").split(',');
			deselectItem(item)
			itemTags.forEach(function(tag){
				(tag.trim() == filter.toLowerCase()) && selectItem(item)
			})
		} else {
			selectItem(item);
		}
	});
	(selectedCount > 0) ? hideDefaultMessage() : showDefaultMessage(); 
	// console.log(selectedCount)
	return(selectedCount)
}

// overlay funcs
var openOverlays = function(overlayRoot){
	NW.Dom.select("body",document,function(bodyElement){
		addClass(bodyElement,'overlay-on')
	})
}

var closeOverlays = function(overlayRoot){
	NW.Dom.select("body",document,function(bodyElement){
		removeClass(bodyElement,'overlay-on')
	})
}

var initOverlay = function(overlaysRoot){
	var navRoot = NW.Dom.select(".overlays-nav", overlaysRoot).shift();
	/*
	removeClass(navRoot,"first")
	removeClass(navRoot,"last")	
	*/
}

var nudgeOverlay = function(overlaysRoot, overlayRoot, direction){
	// move overlay one position 
	// overlayRoot = div.overlay element for the current overlay
	// overlaysRoot = the div.overlays element
	var allOverlays = NW.Dom.select(".overlay",overlaysRoot);
	var thisOverlayIndex = allOverlays.indexOf(overlayRoot);
	var navRoot = NW.Dom.select(".overlays-nav", overlaysRoot).shift();
	console.log("nudgeOverlay: nudging #"+thisOverlayIndex+", direction: "+direction)
	removeClass(navRoot,"first")
	removeClass(navRoot,"last")		
	var nextOverlayIndex = ( (allOverlays.hasOwnProperty(thisOverlayIndex + 1) )? thisOverlayIndex + 1 : thisOverlayIndex );
	(direction > 0) && (nextOverlayIndex+1 == allOverlays.length) && addClass(navRoot,"last")
	var prevOverlayIndex =  (thisOverlayIndex > 0) ? thisOverlayIndex - 1 : thisOverlayIndex ;	
	(direction < 0) && (prevOverlayIndex == 0) && addClass(navRoot,"first")
	if(direction > 0) {
		var targetOverlayIndex = nextOverlayIndex
	} else  {
		var targetOverlayIndex = prevOverlayIndex
	}
	selectOverlayByIndex(overlaysRoot, targetOverlayIndex)
}

var selectOverlayByIndex = function(overlaysRoot, overlayIndex) {
	// manipulates classes to mark overlay as visible
	console.log('selectOverlayByIndex: Selecting '+overlayIndex)
	var allOverlays = NW.Dom.select(".overlay",overlaysRoot,function(overlay){
		removeClass(overlay,"visible")
	})
	addClass(allOverlays[overlayIndex],"visible")
}


var previousOverlay = function(overlaysRoot, overlayRoot){
	// go to it
	nudgeOverlay(overlaysRoot, overlayRoot,-1)
}
var nextOverlay = function(overlaysRoot, overlayRoot){
	// go to it
	nudgeOverlay(overlaysRoot, overlayRoot,1)	
}

// init

window.onload = function(e){
	NW.Dom.select("[slideshow-id]", document, function(slideshowElement){
		console.log("Got slideshow root");
		var slideshowClicked = false
		var slideshowId = slideshowElement.getAttribute('slideshow-id')
		var slideshowPane = NW.Dom.select(".slideshow-pane",slideshowElement).shift();
		var pageCount = NW.Dom.select(".page",slideshowElement).length;
		console.log("... with "+pageCount+" pages")
		var navRoot = NW.Dom.select(".slideshow-nav", slideshowElement).shift();
		navRoot && console.log("found navRoot for slideshow")
		slideshows[slideshowId] = slideshowElement;
		var leftArrowInit = function(leftArrow) {
			console.log("left/back nav arrow found")
			leftArrow.addEventListener('click',function(e){
				var slideshowPanePosition = parseInt(slideshowPane.getAttribute("class").match(/position-([0-9]+)/)[1])
				initNav(navRoot)
				slideshowPanePosition --;
				console.log("user pressed left/back arrow");
				var targetPage = slideshowPanePosition;
				(targetPage <= 1) && ((targetPage = 1) && addClass(navRoot,'first'));
				console.log("target page is "+targetPage);
				var newSlideshowPaneClasses = slideshowPane.getAttribute('class').replace(/position-[0-9]+/,"position-"+targetPage)
				slideshowPane.setAttribute('class',newSlideshowPaneClasses)
				slideshowClicked = true
			},false)
		}
		var rightArrowInit = function(rightArrow) {
			console.log("right/next nav arrow found")
			rightArrow.addEventListener('click',function(e){
				var slideshowPanePosition = parseInt(slideshowPane.getAttribute("class").match(/position-([0-9]+)/)[1])
				initNav(navRoot)
				slideshowPanePosition ++;
				console.log("user pressed right/next arrow");
				var targetPage = slideshowPanePosition;
				(targetPage >= pageCount) && ((targetPage = pageCount) && addClass(navRoot,'last'))
				console.log("target page is "+targetPage);
				var newSlideshowPaneClasses = slideshowPane.getAttribute('class').replace(/position-[0-9]+/,"position-"+targetPage)
				slideshowPane.setAttribute('class',newSlideshowPaneClasses)
				slideshowClicked = true
			},false)
		}
		var initNav = function(navRoot) {
			removeClass(navRoot,'first')
			removeClass(navRoot,'last')			
		}
		addClass(navRoot,'first') // first position assumed on page load
		NW.Dom.select(".arrow.left",navRoot,leftArrowInit);
		NW.Dom.select(".arrow.right",navRoot,rightArrowInit);
		// auto slideshow	
		window.setInterval(function(e){
			if(!slideshowClicked) {
				var slideshowPanePosition = parseInt(slideshowPane.getAttribute("class").match(/position-([0-9]+)/)[1])
				initNav(navRoot)
				slideshowPanePosition ++;
				console.log("auto-slideshow right/next");
				var targetPage = slideshowPanePosition;
				if(targetPage > pageCount) {
					targetPage = 1;
				} else if(targetPage == pageCount) {
					addClass(navRoot,'last')
				} 
				console.log("target page is "+targetPage);
				var newSlideshowPaneClasses = slideshowPane.getAttribute('class').replace(/position-[0-9]+/,"position-"+targetPage)
				slideshowPane.setAttribute('class',newSlideshowPaneClasses)
			}
		},4000)
	})
	/* overlay stuff */
	NW.Dom.select(".overlays",document,function(overlaysRoot){
		/* team portraits clickable */
		NW.Dom.select(".team .squarethumbs",document,function(teamPortraitsRoot){
			var allTeamPortraits = NW.Dom.select(".item",teamPortraitsRoot);
			allTeamPortraits.forEach(function(teamItem){
				teamItem.addEventListener('click',function(e){
					/* use numerical index to find the corresponding overlay */
					var targetIndex = allTeamPortraits.indexOf(teamItem)
					openOverlays(overlaysRoot);
					targetIndex && selectOverlayByIndex(overlaysRoot, targetIndex)
				},false)
			})
		})
		/* general overlay nav */
		NW.Dom.select(".overlays-nav .nav-link",overlaysRoot,function(navArrow){
			// init
			initOverlay(overlaysRoot)
			if(hasClass(navArrow,'left')) {
				navArrow.addEventListener('click',function(e){
					console.log("overlay nav arrow left clicked");
					var visibleOverlayRoot = NW.Dom.select(".overlays .overlay.visible",overlaysRoot).shift();
					previousOverlay(overlaysRoot, visibleOverlayRoot)
				},false)
			} else if(hasClass(navArrow,'right')) {
				navArrow.addEventListener('click',function(e){
					console.log("overlay nav arrow right clicked");
					var visibleOverlayRoot = NW.Dom.select(".overlays .overlay.visible",overlaysRoot).shift();
					nextOverlay(overlaysRoot, visibleOverlayRoot);
				},false)
			} else if(hasClass(navArrow, 'up') || hasClass(navArrow, 'down') || hasClass(navArrow, 'x-btn') ) {
				navArrow.addEventListener('click',function(e){
					console.log("overlay nav arrow up / close clicked");
					closeOverlays(overlaysRoot);
				},false)
			}
		})
	});
	/* news filtering stuff */
	/* the way it is done might be a little-over complicated BUT it is done in a way that allows adding multiple filter selection functionality */
	NW.Dom.select(".news .filter .filter-items",document, function(filterItems){
		// get filter list
		var allFilters = NW.Dom.select(".filter-item-single",filterItems);
		var globalFilterSwitch = NW.Dom.select(".filter-item-all",filterItems).shift();
		allFilters.forEach(function(filter){
			// for each normal filter link
			filter.addEventListener('click',function(activatedFilter){
				// when we click it
				var filterElem = activatedFilter.target;
				var filterName = ( filterElem.hasAttribute('filter-name') ? filterElem.getAttribute('filter-name') : filterElem.innerHTML.trim() );
				allFilters.forEach(function(anyFilter){
					// go through every normal filter link
					if(anyFilter === filterElem) {
						// if it't the one we clicked, set it as selected
						addClass(anyFilter,"selected")
					} else {
						// otherwise unselect it
						removeClass(anyFilter,"selected");				
					}
				})
				// now make sure the "all news" link is not selected
				removeClass(globalFilterSwitch,"selected")
				// and apply the changes
				selectNews(filterName)
			})
		})
		globalFilterSwitch.addEventListener('click',function(activatedGlobalFilterSwitch){
			// now if the "all news" link is clicked
			allFilters.forEach(function(anyFilter){
				// unselect all normal filters
				removeClass(anyFilter,"selected")
			})
			// and make sure the "all news" link is selected
			addClass(activatedGlobalFilterSwitch.target, "selected")
			// last but not least do the work!
			selectNews(false)
		})
		/*
		NW.Dom.select("filter-item-single",filterItems,function(filter){
		});
		*/
	});
};

setContentLoadedCallback(window, function(e){



});