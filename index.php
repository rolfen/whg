<?
	
	require('app.php');

	require(SYSROOT.'lib/htmlhelper.class.php');

	$MAIN = '';

	$MenuItems = array(
		"home"=>"Home",
		"story"=>"Our Story",
		"places"=>"Our Places",
		"team"=>"Team",
		"news"=>"News",
		"contact"=>array("Work with Us","Contact")
	);

	// default landing page
	$p = isset($_GET['p']) ? $_GET['p'] : 'home';

	switch($p) {
		case "home":
			include(USRROOT."blocks/news_items.php");
			include(USRROOT."blocks/home.php");
		break;
		case "story":
			include(USRROOT."blocks/story.php");
			$BIG_FOOTER = "";
			include(USRROOT."blocks/big_footer.php");
		break;
		case "places":
			include(USRROOT."blocks/places.php");
			$BIG_FOOTER = "";
			include(USRROOT."blocks/big_footer.php");
		break;
		case "team":
			include(USRROOT."blocks/team.php");
			$BIG_FOOTER = "";
			include(USRROOT."blocks/big_footer.php");
		break;
		case "contact":
			include(USRROOT."blocks/contact.php");
			$BIG_FOOTER = "";
			include(USRROOT."blocks/big_footer.php");
		break;
		case "news":
			include(USRROOT."blocks/news_items.php");
			include(USRROOT."blocks/news.php");
			$BIG_FOOTER = "";
			include(USRROOT."blocks/big_footer.php");
		break;
		case "die":
			die("The website is not ready yet. Please come back soon.");
		break;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?= HtmlHelper::UTF8() ?>

	<?= HtmlHelper::Favicon("favicon.png") ?>
	<?= HtmlHelper::LinkScript(WEBSYSROOT."js/nwmatcher.min.js") ?>
	<?= HtmlHelper::LinkScript(WEBSYSROOT."js/funcs.js") ?>
	<?= HtmlHelper::LinkScript(WEBSYSROOT."js/slideshow.js") ?>
	<?= HtmlHelper::LinkScript(USRROOT."js/user.js") ?>

	<? /*
	<script type="text/javascript" src="<?=SYSROOT."js/nwmatcher.min.js" ?>"></script>
	<script type="text/javascript" src="<?=SYSROOT."js/funcs.js" ?>"></script>
	<script type="text/javascript" src="<?=SYSROOT."js/slideshow.js" ?>"></script>
	<script type="text/javascript" src="<?=USRROOT."js/user.js" ?>"></script>
	*/ ?>


	<link type="text/css" rel="stylesheet" media="" href="css/fonts.css" />
	<link type="text/css" rel="stylesheet" media="" href="fonts/Lato/Lato-Regular.css" />
	<link type="text/css" rel="stylesheet" media="" href="fonts/Lato/Lato-Bold.css" />
	<link type="text/css" rel="stylesheet" media="" href="fonts/Lato/Lato-Italic.css" />
	<link type="text/css" rel="stylesheet" media="" href="fonts/Lato/Lato-BoldItalic.css" />
	<link type="text/css" rel="stylesheet" media="" href="css/user-compiled.css" />
	<style type="text/css">
	/* temporary css goes here */
	<? if(false or isset($_GET['d'])) { ?>
	body {
		background-image: url('m/md4o.jpg');
		background-position: center top;
		background-repeat: no-repeat;
		background-size: 1090px;
		opacity: .7;
	}
	.slideshow-1 {
		/* just let it be
		max-height: 60vh;
		*/
	}
	<? } ?>
	</style>
	<!-- remind the iPhone that he's a phone -->
	<meta name="viewport" id="viewport" content="width=400px, initial-scale=.8" />
	<title>WHG</title>
</head>
<body class="" ><!-- set body class to overlay-on to show overlay -->
	<div class="wrapper <?= $p ?>">
		<div class="header">
			<a class="name" href="?p=home">
				<span class="look-at-me">Wisors</span> <span class="ktir-mhemm">Hospitality Group</span>			
			</a>
			<div class="menu">
				<div class="khuza3bala">
					<div class="line line1"></div>
					<div class="line line2"></div>
					<div class="line line3"></div>
				</div>
				<div class="items">
					<? foreach($MenuItems as $MenuItemPage => $MenuItem) { ?>
					<? is_array($MenuItem) and $MenuItem = $MenuItem[0] ?>
					<a href="./?p=<?= $MenuItemPage ?>"
						class="item <?= $MenuItemPage ?> <?= $MenuItemPage == $p ? 'selected' : '' ?>"
					>
						<span><?= $MenuItem ?></span>
					</a>
					<? } ?>
				</div>
			</div>
		</div>
		<div class="body">
			<?= $MAIN ?>
		</div>
		<div class="tail">
			<?= isset($BIG_FOOTER) ? $BIG_FOOTER : '' ?>
			<div class="footer">
				<div class="text">
					&copy;2015 Wisors Hospitality All Rights Reserved
				</div>
			</div>			
		</div>
	</div>
</body>
</html>