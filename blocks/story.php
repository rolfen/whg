<? start_block($MAIN) ?>
<div class="slideshow image-slideshow slideshow-1 hideable" slideshow-id="slideshow-1">
	<img class="slideshow-overlay" src="img/slideshow_overlay.png">
	<div class="slideshow-pane position-1">
			<? foreach(scandir($img_basedir = USRROOT."img/home_slideshow") as $file) { 
				$ext = strtolower(substr($file, -4));
				in_array($ext,array('.jpg','.jpeg')) 
				and print("<div class=\"page\"><img class=\"cover\" src=\"${img_basedir}/${file}\"></div>");
				// <img class="cover" src="./img/sample-image.jpg">
				// <img class="cover-overlay" src="./img/we-seek.png">
			} ?>
	</div>
	<div class="slideshow-nav">
		<div class="arrow left"></div>
		<div class="arrow right"></div>
	</div>							
</div>

<div class="entries story-entries">
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2008.jpg" />
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2008</h2>
				<h3>NOVEMBER TO DECEMBER</h3>
				<p>Founders Elie Nehme and Micky Abou 
				Merhy, spent weeks discussing the idea of 
				opening a small bar to hang out with 
				friends after a long day at work.</p>			
			</div>
		</div>
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2009.jpg" />
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2009</h2>
				<h3>January</h3>
				<p>The guys found a small corner space in a charming street of Hamra.</p>
				<h3>JUNE 11</h3>
				<p>First member of the family, “Oscar Wilde Bar” opens its doors. </p>
			</div>
		</div>
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2011.jpg">
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2011</h2>
				<h3>February</h3>
				<p>The “hang-out” place needed more attention.</p>
				<h3>March</h3>
				<p>Elie took a leave from his day time job to organize the operations of Oscar Wilde while testing his life outside the consulting ﬁrm.</p>
				<h3>October</h3>
				<p>The founders quit their jobs, conﬁdent that Oscar Wilde Bar was just their ﬁrst stepping stone in the Hospitality world.</p>
			</div>
		</div>	
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2012_1.jpg">
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2012</h2>
				<h3>June</h3>
				<p>Chapter II was to be written in Mar Mikhayel - a neighborhood district that would later become Beirut’s hot spot</p>
				<h3>August</h3>
				<p>The bartender Yves El Khoury stroke up a friendship that would later turn into a professional partnership with restaurateurs Elie Nehme and Micky AbouMerhy</p>
			</div>
		</div>	
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2012_2.jpg">
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2012</h2>
				<h3>September</h3>
				<p>Santana, the bar manager at Vyvyan’s, joins the family.</p>
				<h3>October 28</h3>
				<p>Elie and Micky launch Vyvyan’s, one of the first bars to open in Mar Mikhayel.</p>
			</div>
		</div>	
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2012_3.jpg">
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2012</h2>
				<h3>October</h3>
				<p>Along with their new partner Yves, the founders set their eyes on a spot in Badaro – a calm neighborhood known for its vibrant past.</p>
				<h3>May</h3>
				<p>The Happy Prince, the family’s first restaurant opens its doors next to Vyvyan’s.</p>
			</div>
		</div>	
	</div>
	<div class="entry">
		<div class="image-box">
			<img class="image" src="img/story/2013.jpg">
		</div>
		<div class="text">
			<div class="text-content">				
				<h2>2013</h2>
				<h3>December</h3>
				<p>The team launches a neighborhood café bar called Kissproof.</p>
			</div>
		</div>	
	</div>
	<div class="entry quicknav">
		<div class="image-box">
			<div class="button go-up" onclick="window.scrollTo(0,0)"><img class="go-up" src="img/thin_up.png" /></div>
		</div>
		<div class="text">
			<div class="text-content">
				<a href="?p=places" class="button">View Our Places</a>
			</div>
		</div>	
	</div>
</div>
<? end_block($MAIN) ?>