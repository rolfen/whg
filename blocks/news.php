<? start_block($MAIN) ?>
<div class="website-page news">
	<div class="content">
		<div class="filter">
			<b class="uc">Filter</b>
			<ul class="filter-items">
				<li class="filter-item-all selected">All News</li>
				<li class="filter-item-single" filter-name="featured">Featured News</li>
				<li class="filter-item-single" filter-name="press">Blogs and Reviews</li>
				<li class="filter-item-single" filter-name="misc">Miscellaneous</li>
			</ul>
		</div>
		<div class="news-items">
			<div class="default-message">Nothing here (yet?)</div>
			<? foreach($NewsItems as $Item)  { ?>
			<div class="item news-item selected" tags="<?= $Item['tags'] ?>">
				<div class="image-container">
					<? if(isset($Item['image'])) { ?>
						<img src="<?= $Item['image'] ?>">
					<? } ?>
				</div>
				<div class="text-container">
					<div class="item-header">
						<span class="item-title"><?= $Item['title'] ?></span>
						<? if(isset($Item['date'])) { ?>
						<span class="item-date"><?= $Item['date'] ?></span>						
						<? } ?>
					</div>
					<p class="item-text">
						<?= $Item['text'] ?>
					</p>
					<? if(isset($Item['link']))  { ?>
					<div class="item-footer">
						<a class="link" href="<?= isset($Item['protocol']) ? $Item['protocol'] : 'http://' ?><?= $Item['link'] ?>" target="_blank"><?= $Item['link'] ?></a>
					</div>
					<? } ?>
				</div>
			</div>
			<? } ?>
		</div>
	</div>
</div>

<? end_block($MAIN) ?>