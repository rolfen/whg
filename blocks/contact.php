<?
$mailBotAddress = "mailer@wisorshospitalitygroup.com";
$mailBotName = "WHG mailer";
$mailRecipients = "h.dergham@gmail.com";
if(isset($_POST) and !empty($_POST)) {
	$stop = $success = false;
	if(empty($_POST['message'])) {
		$postFeedback = "Please write a message first!";
		$stop = true;
	} else if(empty($_POST['name'])) {
		$postFeedback = "Please enter your name.";
		$stop = true;
	} else if(empty($_POST['email'])) {
		$postFeedback = "Please enter an email address.";
		$stop = true;
	}
	if(!$stop) {
		$ret = sendContactMessage($_POST,null,$mailBotAddress,$mailRecipients,$mailBotName);
		$success = $ret['success'];
		// $success or print_r($ret);
	}
}

?>
<? start_block($MAIN) ?>
<div class="website-page contact">
	<div class="section slideshow">
		<img class="page cover" src="img/contact_header.jpg" />
	</div>
	<div class="content cols"><!--
		--><div class="col1 col">
			<? if(isset($_POST) and !empty($_POST)) { ?>
				<p>
					<? if(isset($postFeedback)) { ?>
						<?= $postFeedback ?><br/>
					<? } ?>
					<? if($stop) { ?>
						Your message was not sent.
					<? } else if($success) { ?>
						Your message was sent.
					<? } else { ?>
						Could not send your message.
					<? } ?>
				</p>
			<? } ?>
			<form class="contact-form" method="POST">
				<input placeholder="NAME" type="text" name="name" value="<?= @$_POST['name'] ?>"></input>
				<input placeholder="EMAIL" type="email" name="email" value="<?= @$_POST['email'] ?>"></input>
				<textarea placeholder="MESSAGE" name="message"><?= @$_POST['message'] ?></textarea>
				<input type="submit" class="button" value="Send Message">
			</form>
		</div><!--
		--><div class="col2 col"><!--
			--><div class="col2-1 col">
				<h3>Keep in Touch</h3>
				<p>Use our contact form and we'll get back at you as humanly as possible.</p>
				<div class="places-list">
					<div class="place">
						<div class="place-name">Oscar Wilde</div>
						<div class="place-contact">+961 1 456 439</div>
					</div>
					<div class="place">
						<div class="place-name">Vyvyans</div>						
						<div class="place-contact">+961 1 456 439</div>
					</div>
					<div class="place">
						<div class="place-name">The Happy Prince</div>						
						<div class="place-contact">+961 1 456 439</div>
					</div>
					<div class="place">
						<div class="place-name">Kissproof</div>
						<div class="place-contact">+961 1 456 439</div>
					</div>
				</div>
			</div><!--
			--><div class="col2-2 col">
				<h3>Job Openings</h3>
				<p>Stay tuned!</p>
				<p>For any specific request please use the contact form on this page, or call us.</p>
				<!--
				<div class="job-title">Bar Manger @ Kissproof</div>
				<p>
					20 - 40 years old,<br/>
					8 years experience
				</p>
				<div class="job-title">Kitchen Porter @ Kissproof</div>
				<p>
					20 - 140 years old.<br/>
					3 years experience
				</p>
				<p>
					<div class="job-title">Busboy @ THP</div>
					18 - 80 years old.<Br/>
					 2 years experience
				</p>	
				-->
			</div><!--
		--></div><!--
	--></div>
</div>

<? end_block($MAIN) ?>
