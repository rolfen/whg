<? start_block($MAIN) ?>

			<div class="section">
				<div class="slideshow image-slideshow slideshow-1" slideshow-id="slideshow-1">
					<img class="slideshow-overlay" src="img/slideshow_overlay.png">
					<div class="slideshow-pane position-1">
							<? foreach(scandir($img_basedir = USRROOT."img/home_slideshow") as $file) { 
								$ext = strtolower(substr($file, -4));
								in_array($ext,array('.jpg','.jpeg')) 
								and print("<div class=\"page\"><img class=\"cover\" src=\"${img_basedir}/${file}\"></div>");
								// <img class="cover" src="./img/sample-image.jpg">
								// <img class="cover-overlay" src="./img/we-seek.png">
							} ?>
					</div>
					<div class="slideshow-nav">
						<div class="arrow left"></div>
						<div class="arrow right"></div>
					</div>							
				</div>
			</div>
			<div class="section intro-text">
				<p>
					It all started small. Since opening “Oscar Wilde” in 2010,
					our family has grown to include three more members:
					“Vyvyan’s”, “The Happy Prince”, and “Kissproof”.
					We’ve had a thrilling journey, and we can’t wait
					to see where it leads us next.
				</p>
				<a href="?p=story" class="button">
					Our Story
				</a>
			</div>
			<div class="section gray-ish" slideshow-id="slideshow-2">
				<div class="slideshow desktop-only slideshow-2">
					<div class="slideshow-pane position-1">
						<? foreach($NewsItems as $i => $item)  { ?>
						<? if($i < 5) { ?>
						<div class="page has-image">
							<div class="news-item content">
								<div class="image-container optional-image">
									<? if(isset($item['image'])) { ?>
										<img src="<?= $item['image'] ?>">
									<? } ?>
								</div>
								<div class="main-text">
									<p>
										<b>
											<?= @ $item['title'] ?>
											<?= @ $item['date'] ?>
										</b><br/>
											<?= @ $item['text'] ?>
										<br/>
										<a class="link" href="<?= isset($item['protocol']) ? $item['protocol'] : 'http://' ?><?= $item['link'] ?>" target="_blank"><?= $item['link'] ?></a>
									</p>							
								</div>								
							</div>
						</div>
						<? } ?>
						<? } ?>
					</div>
				</div>
				<div class="slideshow-nav">
					<div class="arrow left"></div>
					<div class="arrow right"></div>
				</div>
				<a href="?p=news" class="button">
					View More News
				</a>
			</div>
			<div class="section branch-logos"><!--
				--><a href="/oscarwilde/" target="_self"><img src="img/oscar_logo.png"></a><!--
				--><a href="/vyvyans/" target="_self"><img src="img/vyvians_logo.png"></a><!--
				--><a href="/thehappyprince/" target="_self"><img src="img/thp_logo.png"></a><!--
				--><a href="/kissproof/" target="_self"><img src="img/kp_logo.png"></a><!--
			--></div>
<? end_block($MAIN) ?>