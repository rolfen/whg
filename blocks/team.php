<? start_block($MAIN) ?>
<div class="slideshow image-slideshow slideshow-1 hideable" slideshow-id="slideshow-1">
	<div class="slideshow-pane position-1">
			<? foreach(scandir($img_basedir = USRROOT."img/team_slideshow") as $file) { 
				$ext = strtolower(substr($file, -4));
				in_array($ext,array('.jpg','.jpeg')) 
				and print("<div class=\"page\"><img class=\"cover\" src=\"${img_basedir}/${file}\"></div>");
				// <img class="cover" src="./img/sample-image.jpg">
				// <img class="cover-overlay" src="./img/we-seek.png">
			} ?>
	</div>
	<div class="slideshow-nav">
		<div class="arrow left"></div>
		<div class="arrow right"></div>
	</div>							
</div>
<div class="team">
	<div class="items squarethumbs"><!--
		--><div class="item">
			<div class="portrait-box">
				<img src="img/pipol/elie_nehme.jpg">
			</div>
			<div class="caption-box">
				<div class="name">Elie Nehme</div>
				<div class="title">Co Founder <br/> Executive Chef</div>
			</div>
		</div><!--
		--><div class="item">
			<div class="portrait-box">
				<img src="img/pipol/michel_abou_merhi.jpg">
			</div>
			<div class="caption-box">
				<div class="name">Michel Abou&nbsp;Merhi</div>
				<div class="title">Co Founder</div>
			</div>
		</div><!--
		--><div class="item">
			<div class="portrait-box">
				<img src="img/pipol/elie_bou_moussa.jpg">
			</div>
			<div class="caption-box">
				<div class="name">Elie Bou Moussa</div>
				<div class="title">
					<span class="nc">aka</span> Santana
				</div>
			</div>
		</div><!--
		--><div class="item">
			<div class="portrait-box">
				<img src="img/pipol/yves_el_khoury.jpg">
			</div>
			<div class="caption-box">
				<div class="name">Yves El&nbsp;Khoury</div>
				<div class="title">
					<span class="nc">aka Crazy Old Tom</span><br/>
					Bar Manager
				</div>
			</div>
		</div><!--
		--><div class="item">
			<div class="portrait-box">
				<img src="img/pipol/robert_mansour.jpg">
			</div>
			<div class="caption-box">
				<div class="name">
					Rober Mansour
				</div>
				<div class="title">
					<span class="nc">aka left. Man’sæoūr</span><br/>
					Chef De Cuisine
				</div>
			</div>
		</div><!--
	--></div>
	<div class="overlays bios">
<? /* template
		<div class="overlay">
			<div class="page-box">
				<span class="name"></span>
				<span class="alias"></span><br/>
				<span class="title"></span>
				<p></p>
			</div>
		</div>
*/ ?>
		<div class="overlay visible">
			<div class="page-box">
				<div class="title-box">
					<span class="name">Elie Nehme</span>
					<span class="alias">Mr. White</span><br/>
					<span class="title">Executive Chef/Co-G.O.D</span>
				</div>
				<p>Believe it or not the Chef behind the masterworks is self-taught, Elie Nehme never attended culinary school. He developed his passion for cooking while growing up, opening his home to a series of pop-up dinners and get-togethers. Since those days Elie’s come a long way.
 				<p>After earning his B.E in Civil Engineering, Elie worked for a couple of years as a consultant in an international strategy firm, before deciding to shift his career path, beginning a lifelong journey pursuing his true passion: Cooking. While the change may have shocked his friends and family at that time, the hospitality scene is all the better for it. His cooking style reflects a wide range of cuisines with a common focus on experimenting aggressive flavors- the way he loves to eat. Despite the claims that he’s a hardheaded chef for not honoring special requests, Elie’s cooking lead to the places’ immediate popularity and critical success. His quick hot-tempered behavior in the kitchen contrasts in real life his genuine trademark outburst laughs. In his spare time, Elie loves sitting at the bar or standing behind it, mixing his favorite ingredients and experimenting new creations.</p>
				<p>Besides serving as the group’s Executive Chef, Elie is part of the group’s leadership team. With his strong knowledge in corporate structure and development, he is continuously fostering the family’s strategy and culture.</p>
			</div>
		</div>
		<div class="overlay">
			<div class="page-box">
				<div class="title-box">
					<span class="name">Micky Abou Merhy</span>
					<span class="alias">The guy in the Tahoe</span><br/>
					<span class="title">Co-Founder</span>
				</div>
				<p>Micky has been involved in the nightlife scene since he was still a minor. He spent several years working in local nightclubs as a manager of public relations developing his social and communication skills. While his partner bought his own sports car at the age of 19, Micky didn't save a penny as he spent it all in Beirut’s trendiest bars and clubs. As a fresh graduate with a degree in economics, Micky chose a path that has taken him away from his country, aiming an M.B.A in one of the top grandes école in France. Instead of studying, he spent most of his time road tripping across Europe, visiting the continent’s most vibrant cities. Keen on coming back home, Micky earned his MBA and started working as a sales and marketing manager in a spirit distribution company. During his time there, he worked in collaboration with international companies strengthening the positioning of his brands in the market. He also mastered the art of negotiation by closing deals with bar owners as a daily routine, which fueled his desire to open his own place Oscar Wilde Bar. Micky is part of the group’s leadership team; with his emotional intelligence he plays a major role developing the operational and front of house systems for the group.</p>
			</div>
		</div>
		<div class="overlay">
			<div class="page-box">
				<div class="title-box">
					<span class="name">Elie Bou Moussa</span>
					<span class="alias">Santana</span><br/>
					<span class="title">Unknown</span>
				</div>
				<p>While other kids wanted to be firemen and superheroes, bartenders wanted to be Santanas. He has been hooked on cocktails since he started his first job at the age of 18 in a five stars hotel, serving drinks to the wealthy well-heeled international crowd. After spending 4 years there, Santana set his eyes on the night life scene, as he couldn't stand waking up for the grumpy morning commute. He spent years working in late night bars, developing his passion for cocktails and history. He joined the group in 2012, and since the day he set his first steps behind Vyvyan’s bar he never left it. The place’s raw walls and rusty electric pipes are now home for him. Santana’s genuine attitude and charisma proves that bartending is not only his profession but also his vocation. He became heavily immersed in the cocktail culture and continues to show great respect to the classics while finding new creations in modern ingredients and techniques. His famous “Parallel Universe” has earned local and global recognition, a creative approach to drink making, using ingredients like grilled meat and cherry tomatoes. He now serves as Vyvyan’s head bartender and Mixologist.</p>
			</div>
		</div>
		<div class="overlay">
			<div class="page-box">
				<div class="title-box">
					<span class="name">Yves Khoury</span>
					<span class="alias">Crazy Old Tom</span><br/>
					<span class="title">Bar Manager</span>
				</div>
				<p>Born and raised in a Christian family, Yves used to visit the church every Sunday. While his brother grew up to be a Catholic priest, it turned out Yves was only there for the communion wine, as his impressive average was 4 to 5 communions per Mass! Yves didn’t wait long to start following his passion for wines evolving towards spirits, cocktails and shitfaces. He started working in restaurants in his early teens, and worked his way up from a food runner in the cheapest beach resort to a bartender at one of the hottest bars in Beirut. After years spent learning the craft of classic cocktails and mastering his overall techniques, his talent spotted restaurateurs Elie Nehme and Micky Abou Merhy. Yves joined the family and spent his first months learning a new approach on modern mixology, improving his knowledge and affinity for well-crafted cocktails. Later on, he was given the opportunity to be along with his new partners part of the design, setup and management of what is known today as Kissproof. Yves’s technical skills and craftsmanship coupled with his inherent hospitality and warm nature is what sets him apart from others in this field. He now serves as Kissproof’s mixologist and Bar Manager.</p>
			</div>
		</div>
		<div class="overlay">
			<div class="page-box">
				<div class="title-box">
					<span class="name">Robert Mansour</span>
					<span class="alias">Lieutenant Man’sæoūr</span><br/>
					<span class="title"></span>
				</div>
				<p>
					Robert grew up in a typical countryside family who lived in a land of little rain and perfect sun where he spent most of his childhood hunting and practicing farming rituals. After taking several beatings from his father and elder brothers for not being a talented dairy milker, Robert took his hunting rifle on a new adventure, and moved out from his country home to the city. At the age of 16, Robert enrolled in a culinary institute and entered the restaurant industry, moving from dishwasher, to busboy to sous-chef in a local restaurant. Few years later, he headed to the gulf and worked for six years in an international hotel where he participated to different exchange programs, practicing different types of global cuisine. Robert joined The Happy Prince’s kitchen in 2012, and has since then been handling its operations. Robert’s enthusiastic cooking and tough personality along with his affectionate method of embracing raw meat while prep-ing it clearly reflects the imprint that was left by his parents on him: devotion for farming lands and fondness for its animals. Robert serves as The Happy Prince and Kissproof’s Chef de Cuisine, and lives in Beirut with his wife, two daughters and old-time hunting rifle.    
				</p>
			</div>
		</div>
		<div class="overlays-nav">
			<div class="nav-link arrow right"></div>
			<div class="nav-link arrow left"></div>
			<div class="nav-link x-btn"></div>
		</div>
	</div>
</div>
<? end_block($MAIN) ?>
