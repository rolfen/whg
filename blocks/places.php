<? start_block($MAIN) ?>
<div class="big-thumbs places items">
	<a href="" class="item">
		<div class="bg"></div>
		<div class="caption">
			A feel-good cocktail bar hallowing the classics in the eclectic Hamra alleyway
		</div>
	</a>
	<a href="" class="item">
		<div class="bg"></div>
		<div class="caption">
			A modern gastro-bar, combining rustic cooking and creative cocktails, in a casual and vibrant setting
		</div>
	</a>
	<a href="" class="item">
		<div class="bg"></div>
		<div class="caption">
			A casual bustling bar, focused on serving quality spirits and cocktails with integrity and passion
		</div>
	</a>
	<a href="" class="item">
		<div class="bg"></div>
		<div class="caption">
			An all-day neighborhood mainstay, serving sophisticated bar-food and elegant cocktails, in a laidback and friendly environment
		</div>
	</a>
</div>

<? end_block($MAIN) ?> 
