<? start_block($BIG_FOOTER) ?>
	<div class="big-footer">
		<div class="contents">
			<div class="col">
				<a href="./" target="_self" class="name">
					<span class="look-at-me">Wisors <br/></span>
					<span class="ktir-mhemm">Hospitality <br/>Group</span>			
				</a>		
			</div>
			<div class="places">
				<div class="col">
					<a href="/oscarwilde/" target="_self" class="place">
						<div class="place-name">Oscar Wilde</div>
						<div class="place-contact">+961 1 456 439</div>
					</a>
					<a href="/vyvyans/" target="_self" class="place">
						<div class="place-name">Vyvyans</div>						
						<div class="place-contact">+961 1 456 439</div>
					</a>
				</div>
				<div class="col">
					<a href="/thehappyprince/" target="_self" class="place">
						<div class="place-name">The Happy Prince</div>						
						<div class="place-contact">+961 1 456 439</div>
					</a>
					<a href="/kissproof/" target="_self" class="place">
						<div class="place-name">Kissproof</div>
						<div class="place-contact">+961 1 456 439</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="bottom-menu">
					<div class="items">
						<? foreach($MenuItems as $MenuItemPage => $MenuItem) { ?>
						<? is_array($MenuItem) and $MenuItem = $MenuItem[1] ?>
						<a href="./?p=<?= $MenuItemPage ?>"
							class="item <?= $MenuItemPage ?> <?= $MenuItemPage == $p ? 'selected' : '' ?>"
							>
							<span><?= $MenuItem ?></span>
						</a>
						<? } ?>
					</div>
				</div>
			</div>				
		</div>
	</div>
<? end_block($BIG_FOOTER) ?>