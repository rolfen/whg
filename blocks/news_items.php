<?
$NewsItems = array(
	/* 
	 * See syntax for a news item in example below (YOU DON'T NEED TO WRITE ALL PROPERTIES, usually text, title and tags is enough)
	 * Watch out for double quotes inside the text, it will break the code.
	 * If you have double quotes inside the text neutralize them like this: \"

	array(
		"title" => "Lebanese Bacon Awards",
		"date" => "20.10.2015",
		"image" => "img/myface1.jpg",
		"tags" => "featured,press",
		"text" => "Capicola kevin biltong, t-bone ham ground round spare ribs sirloin shank jerky short loin doner picanha. Chuck ham hock beef kevin ham venison. loin strip steak ground pork chop shankle salami round ﬂank cupim andouille.",
		"protocol" => "http://", // "http://" is the default if not defined
		"link" => "google.com", 
	),

	*/
	array(
		"title" => "VYV // L’hote Libanais",
		"image" => "img/news/01.jpg",
		"tags" => "featured,press,vyv",
		"text" => "Someone shouts my name over half a dozen competing conversations. My Whiskey Sour is ready and I have to maneuver through the crowd pouring out of Vyvyan’s, taking good care not to step on any toes or to knock out any drinks…",
		"link" => "www.hotelibanais.com/article/beirut-mar-mikhael/",
	),
	array(
		"title" => "THP // Maia’s restaurant review",
		"image" => "img/news/02.png",
		"tags" => "featured,press,thp",
		"text" => "Onslaught aside, we all really enjoyed the meal at The Happy Prince. This restaurant bar serves some impressive food. We started the meal off with the artichoke dip, the ahi tuna, and the chicken salad. Bubbly, cheesy, and super rich…",
		"link" => "www.nadsreviews.com/2013/06/maias-restaurant-review-happy-prince.html",
	),
	array(
		"title" => "THP // Chocolate and Vanilla Sole",
		"image" => "img/news/03.png",
		"tags" => "press,thp",
		"text" => "Full is more like it. That’s the way I felt when I finished my delicious meal at the bar/restaurant in the famous Mar Mikhael Street in Beirut, The Happy Prince. The restaurant has become very known by word of mouth due to its DELICSIOUS dishes…",
		"link" => "chocolateandvanillasoles.com/2013/10/07/the-happy-prince/",
	),
	array(
		"title" => "KP //  CNNGo",
		"image" => "img/news/04.jpg",
		"tags" => "featured,press,kp",
		"text" => "This small bar is one of the many new additions to Beirut's nightlife. It's located in the Badaro district, once a sleepy residential area and now a buzzing favorite among those in the know with a wave of new openings. Not only a place for a cocktail but also a chess battleground among locals…",
		"link" => "edition.cnn.com/2015/05/13/travel/cnngo-beirut-travel/",
	),
	array(
		"title" => "KP // Le Commerce du Levant",
		"image" => "img/news/05.jpg",
		"tags" => "press,kp",
		"text" => "Kissproof, un nouveau concept de restauration s’est installé rue Badaro en décembre. L’établissement est un « bar-café, parfaitement adapté à Badaro qui accueille de plus en plus de bureaux et de jeunes résidents…",
		"link" => "www.lecommercedulevant.com/affaires/h%C3%B4tellerie-amp-tourisme/none-liban/badaro-accueille-kissproof-un-bar-caf%C3%A9-de-quartier/22936",
	),
	array(
		"title" => "KP // Gino’s Blog",
		"image" => "img/news/06.jpg",
		"tags" => "press,kp",
		"text" => "One new Beiruti area that has been quietly but surely rising to the top, is Badaro. With delightful venues like Kissproof always full, and a handful of others popping up on the Badaro main street…",
		"link" => "ginosblog.com/2014/06/02/why-badaro-could-be-the-awesomest-new-hotspot/",
	),
	array(
		"title" => "Negroni Week",
		"image" => "img/news/07.jpg",
		"tags" => "featured,news",
		"text" => "at Vyvyan’s, Kisproof, Oscar Wilde, and Vyvyan’s. All proceeds went to the Children Cancer Center, Lebanon.",
		"link" => "negroniweek.com/ ",
	),
);
?>
